// jQuery(document).ready(function(e) {
//     var WindowHeight = jQuery(window).height();

//     var load_element = 0;

//     //position of element
//     var scroll_position = jQuery('.animated').offset().top;

//     var screen_height = jQuery(window).height();
//     var activation_offset = 0;
//     var max_scroll_height = jQuery('body').height() + screen_height;

//     var scroll_activation_point = scroll_position - (screen_height - activation_offset);

//     jQuery(window).on('scroll', function(e) {

//         var y_scroll_pos = window.pageYOffset;
//         var element_in_view = y_scroll_pos > scroll_activation_point;
//         var has_reached_bottom_of_page = max_scroll_height <= y_scroll_pos && !element_in_view;

//         if (element_in_view || has_reached_bottom_of_page) {
//             jQuery('.animated').addClass("fadeInUp");
//         } else {
//             jQuery('.animated').removeClass("fadeInUp");
//         }

//     });

// });

// Adiciona a class de animação no html
var root = document.documentElement;
root.className += ' js';
idBox = document.querySelector("html")

function boxTop(idBox) {
    var boxOffset = $(idBox).offset().top;
    return boxOffset;
}

$(document).ready(function() {
    var $target = $('.animated'),
        animationClass = 'fadeInUp',
        windowHeight = $(window).height(),
        offset = windowHeight - (windowHeight - 650  );

    function animeScroll() {
        var documentTop = $(document).scrollTop();
        $target.each(function() {
            if (documentTop < boxTop(this) - offset) {
                // $(this).removeClass(animationClass);
                return;
            } else {
                $(this).addClass(animationClass);
            }
        });
    }
    animeScroll();

    $(document).scroll(function() {
        setTimeout(function() { animeScroll() }, 1);
    });
});

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();

        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});
